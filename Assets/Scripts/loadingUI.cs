﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loadingUI : MonoBehaviour
{
    public Text txt;
    public InputField input;

    // Start is called before the first frame update
    void Start()
    {
        this.txt.text = "Welcome please chose a name:";
        this.input.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnEndEdit(string pName)
    {
        Player.GetInstance().playerName = pName;
        this.txt.text = "Welcome " + pName;
        this.input.gameObject.SetActive(false);
        Invoke("startGame", 0.5f);
    }

    void startGame()
    {
		SceneManager.LoadScene("main");
    }
}
