using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{
    public BrickBase BrickPrefab;
    public BrickBase BrickRoundPrefab;
    public int LineCount = 6;
    public Rigidbody Ball;

    public Text ScoreText;
    public Text PNameTxt;
    public Text BestScoreTxt;
    public GameObject GameOverText;
    
    private bool m_Started = false;
    private int m_Points;
    
    private bool m_GameOver = false;

    
    // Start is called before the first frame update
    void Start()
    {
        const float step = 0.6f;
        int perLine = Mathf.FloorToInt(4.0f / step);
        BrickBase prefabToInstantiate;

        for (int i = 0; i < LineCount; ++i)
        {
            for (int x = 0; x < perLine; ++x)
            {
                Vector3 position = new Vector3(-1.5f + step * x, 2.5f + i * 0.3f, 0);
                prefabToInstantiate = ( Random.Range(0, 2) == 0 ) ? this.BrickPrefab : this.BrickRoundPrefab;
                var brick = Instantiate(prefabToInstantiate, position, Quaternion.identity);
                brick.onDestroyed.AddListener(AddPoint);
            }
        }


       string pName = Player.GetInstance().playerName;
       if(pName != null && pName.Contains("-1"))
       {
           this.PNameTxt.gameObject.SetActive(false);
       }
       else
       {
           this.PNameTxt.text = pName;
           this.PNameTxt.gameObject.SetActive(true);
       }

       int bestScore            = PlayerPrefs.GetInt("BestScore", 0);
       string bestScorePName    = PlayerPrefs.GetString("BestScoreName", "-1");

       if(bestScorePName.Contains("-1"))
            this.BestScoreTxt.text = "Best Score:" + bestScore;
        else
            this.BestScoreTxt.text = "Best Score " + bestScorePName+ ": " + bestScore;
    }

    private void Update()
    {
        if (!m_Started)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_Started = true;
                float randomDirection = Random.Range(-1.0f, 1.0f);
                Vector3 forceDir = new Vector3(randomDirection, 1, 0);
                forceDir.Normalize();

                Ball.transform.SetParent(null);
                Ball.AddForce(forceDir * 2.0f, ForceMode.VelocityChange);
            }
        }
        else if (m_GameOver)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    void AddPoint(int point)
    {
        m_Points += point;
        ScoreText.text = $"Score : {m_Points}";
    }

    public void GameOver()
    {
        // check bestscore
       int bestScore = PlayerPrefs.GetInt("BestScore", 0);
        if(this.m_Points >bestScore)
        {
            PlayerPrefs.SetString("BestScoreName", Player.GetInstance().playerName);
            PlayerPrefs.SetInt("BestScore", m_Points);
            this.BestScoreTxt.text = "Best Score " + Player.GetInstance().playerName+ ": " + m_Points;
        }

        // reset points
        this.m_Points = 0;
        m_GameOver = true;
        GameOverText.SetActive(true);
    }
}
