using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BrickBase : MonoBehaviour
{
    public UnityEvent<int> onDestroyed;
    
    protected int pointValue = 1;
    protected int lifeValue = 3;

    protected virtual void Awake()
    {
        this.pointValue = 1;
        this.pointValue = 3;
    }

    void Start()
    {
        this._manageLifeColor();
    }

    protected virtual void OnCollisionEnter(Collision other)
    {
        this.lifeValue --;
        this._manageLifeColor();
        if(this.lifeValue > 0)
            return;
        onDestroyed.Invoke(this.pointValue);
        
        //slight delay to be sure the ball have time to bounce
        Destroy(gameObject, 0.2f);
    }

    protected virtual void _manageLifeColor()
    {
        var renderer = GetComponent<Renderer>();

        MaterialPropertyBlock block = new MaterialPropertyBlock();
        switch (this.lifeValue)
        {
            case 0 :
                block.SetColor("_BaseColor", new Color(1f, 0.2078432f, 0f) );
                break;
            case 1:
                block.SetColor("_BaseColor", new Color(0.9960785f, 0.4352942f, 0f) );
                break;
            case 2:
                block.SetColor("_BaseColor", new Color(0.9960785f, 0.6196079f, 0.007843138f) );
                break;
            case 3:
                block.SetColor("_BaseColor", new Color(1f, 0.6980392f, 0.003921569f) );
                break;
            case 4:
                block.SetColor("_BaseColor", new Color(1f, 0.7725491f, 0f) );
                break;
            default:
                block.SetColor("_BaseColor", Color.red);
                break;
        }
        renderer.SetPropertyBlock(block);
    }

}
