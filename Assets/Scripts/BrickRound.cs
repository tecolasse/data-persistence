using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BrickRound : BrickBase
{
    protected override void Awake()
    {
        this.pointValue = 5;
        this.pointValue = 8;
    }

    protected override void _manageLifeColor()
    {
        var renderer = GetComponent<Renderer>();

        MaterialPropertyBlock block = new MaterialPropertyBlock();
        switch (this.lifeValue)
        {
            case 0 :
                block.SetColor("_BaseColor", new Color(1f, 0.2078432f, 0f) );
                break;
            case 1 :
                block.SetColor("_BaseColor", new Color(0.9960785f, 0.2588235f, 0.003921569f) );
                break;
            case 2:
                block.SetColor("_BaseColor", new Color(0.9960785f, 0.4352942f, 0f) );
                break;
            case 3:
                block.SetColor("_BaseColor", new Color(0.9960785f, 0.5333334f, 0f) );
                break;
            case 4:
                block.SetColor("_BaseColor", new Color(0.9960785f, 0.6196079f, 0.007843138f) );
                break;
            case 5:
                block.SetColor("_BaseColor", new Color(1f, 0.654902f, 0f) );
                break;
            case 6:
                block.SetColor("_BaseColor", new Color(1f, 0.6980392f, 0.003921569f) );
                break;
            case 7:
                block.SetColor("_BaseColor", new Color(1f, 0.7372549f, 0f) );
                break;
            case 8:
                block.SetColor("_BaseColor", new Color(1f, 0.7725491f, 0f) );
                break;
            default:
                block.SetColor("_BaseColor", Color.red);
                break;
        }
        renderer.SetPropertyBlock(block);
    }
}